
1. Why use containers and not deploying using traditional methods?
2. What is a container? What is its advantages?
3. Give me an example of an application setup that is suitable to be containerized
4. Explain briefly Container Architecture
5. What is a container image?
6. What is Kubernetes? Why use it?
7. What is the smallest managable unit in Kubernetes?
8. What is a pod?
9. Please give me some Kubernetes Features
10. What is OpenShift?
11. How do I deploy an application using podman?
12. What are the states of a container? What happens to my app when the container dies?
13. How do I run a command in a container? How do I list them?
14. What is Persistent Storage? Why should I use it? How can I use it? What happens to data not saved in Persistent Storage?
15. How does networking works on the host level?
16. test?
