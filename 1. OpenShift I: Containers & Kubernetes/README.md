# Red Hat OpenShift I: Containers & Kubernetes

### Chapter 1-3
- Introducing Container Technology
- Creating Containerized Services
- Managing Containers

### Chapter 4-5
- Managing Container Images
- Creating Custom Container Images

### Chapter 6
- Deploying Containerized Applications on OpenShift

### Chapter 7-8
- Deploying Multi-Container Applications
- Troubleshooting Containerized Applications
