
1. What's a service account?
2. What's a group?
3. What's a role? a cluster role?
4. What 2 methods are there to authenticate with the API?
5. What's HTPasswd?
6. Can I define multiple identity providers?
7. How do I login to a newly created cluster?
8. What happens if I lose the kubeconfig file?
9. What's a rolebinding?
10. Give me some examples for default roles
11. What's a system user?
12. How do I remove the ability for users to create projects?
13. How can I mount a secret?
14. I have a running application with a mounted secret, and I want to change it. What should I do?
15. What's a configmap? When should I use it? 
16. What's scc?
17. How do I assign a different scc to a pod?