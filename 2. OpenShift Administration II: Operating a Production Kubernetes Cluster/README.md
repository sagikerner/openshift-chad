# Red Hat OpenShift Administration II: Operating a Production Kubernetes Cluster

### Chapter 1-2
- Describing the Red Hat OpenShift Container Platform
- Verifying the Health of a Cluster

### Chapter 3-4
- Configuring Authentication and Authorization
- Configuring Application Security

### Chapter 5
- Configuring OpenShift Networking for Applications
