
1. What's SDN? What its purpose?
2. What's CNI?
3. What's OVS?
4. What's the DNS operator? What does it do?
5. What's the Cluster Network Operator?
6. What's Multus CNI?
7. What do I do in case of "Application is not available"?
8. What methods are there for managing ingress traffic?
9. What methods there are to secure a route?
10. What are network policies?
11. Explain the OpenShift Scheduler Algorithm
12. What's a node selector? affinity?
13. What are Resource requests? Limits?
14. What can I define in a quota?
15. What's a limit range?
16. How can I apply a quota to multiple projects?
17. What's the default project template?
18. Scaling pods
19. Explain briefly about cluster updates
