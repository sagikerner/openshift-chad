
1. What's kustomization?
2. How do I extract information from openshift resources(jsonpath, jq)
3. How do I wait on a deployment update?
4. How do I login to a cluster with an automation?
5. Explain about jobs and cronjobs
6. How do I use the OpenShift API? and login to it with curl?
7. explain about k8s modules
8. what are controllers?
9. How do I install an operator?
10. Explain about the states of cluster operators
