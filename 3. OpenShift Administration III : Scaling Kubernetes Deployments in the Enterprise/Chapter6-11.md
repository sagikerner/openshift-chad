
1. What's the Default Wildcard Certificate? How can I change it?
2. What's the Master API certificate? How do I replace it?
3. How to add Additional Trusted Certificate Authorities?
4. What do I do in case of "The server is using an invalid certificate: x509: certificate has expired or is not yet valid"?
5. Where the pods of the Ingress Controller and the api server run?
6. What's ignition?
7. How do I add another worker node?
8. What's the Machine Config Operator?
9. What's MC? MCP?
10. How the machine config selects nodes?
11. What do I configure in a machine config?
12. What's defaultNodeSelector? Can I change it?
13. How do I observe mcp updates?
14. What's persistentVolumeReclaimPolicy?
15. What Access Modes exists?
16. Explain about the local storage operator
17. Explain about the Cluster Monitoring Stack(Prometheus, AlertManager, Grafana, Thanos)
18. What states exist for an alert?
19. Can I export alerts from the cluster?
20. How do I configure storage for the monitoring stack? Why would I do that?
21. How do I check resource usage in a node?
22. What taints can be configured on a node?
23. Explain the Causes for Worker Node Storage Exhaustion
24. What are Other Worker Node Capacity Concerns?
25. A node is not ready. What do I do?
