# Red Hat OpenShift Administration III : Scaling Kubernetes Deployments in the Enterprise

### Chapter 1-3
- Moving From Kubernetes to OpenShift
- Introducing Automation with OpenShift
- Managing OpenShift Operators

### Chapter 6-11

- Configuring Trusted TLS Certificates
- Configuring Dedicated Node Pools
- Configuring Persistent Storage
- Managing Cluster Monitoring and Metrics
- Provisioning and Inspecting Cluster Logging
- Recovering Failed Worker Nodes
